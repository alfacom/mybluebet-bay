//Contains the rapid piping device.
/obj/item/weapon/pipe_dispenser
	name = "Rapid Piping Device (RPD)"
	desc = "A device used to rapidly pipe things."
	icon = 'icons/obj/items.dmi'
	icon_state = "rcd"
	opacity = 0
	density = 0
	anchored = 0.0
	flags = CONDUCT
	force = 10.0
	throwforce = 10.0
	throw_speed = 1
	throw_range = 5
	w_class = 3.0
	matter = list("metal" = 75000, "glass" = 37500)
	origin_tech = "engineering=4;materials=2"
	var/datum/effect/effect/system/spark_spread/spark_system
	var/working = 0
	var/wait = 0
	var/p_type = 0
	var/p_conntype = 0
	var/p_dir = 1
	var/p_class = 0
	var/p_disposal = 0

/obj/item/weapon/pipe_dispenser/New()
	. = ..()
	spark_system = new /datum/effect/effect/system/spark_spread
	spark_system.set_up(5, 0, src)
	spark_system.attach(src)

/obj/item/weapon/pipe_dispenser/attack_self(mob/user as mob)
	if(!user || !src)	return 0
	var/dat = {"<h2>Type</h2>
<b>Regular pipes:</b><BR>
<ul>
	<li><A href='?src=\ref[src];make=0;dir=1'>Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=1;dir=5'>Bent Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=5;dir=1'>Manifold</A><BR></li>
	<li><A href='?src=\ref[src];make=8;dir=1'>Manual Valve</A><BR></li>
	<li><A href='?src=\ref[src];make=20;dir=1'>Pipe Cap</A><BR></li>
	<li><A href='?src=\ref[src];make=19;dir=1'>4-Way Manifold</A><BR></li>
	<li><A href='?src=\ref[src];make=18;dir=1'>Manual T-Valve</A><BR></li>
	<li><A href='?src=\ref[src];make=43;dir=1'>Manual T-Valve - Mirrored</A><BR></li>
	<li><A href='?src=\ref[src];make=21;dir=1'>Upward Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=22;dir=1'>Downward Pipe</A><BR></li>
</ul>
<b>Supply pipes:</b><BR>
<ul>
	<li><A href='?src=\ref[src];make=29;dir=1'>Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=30;dir=5'>Bent Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=33;dir=1'>Manifold</A><BR></li>
	<li><A href='?src=\ref[src];make=41;dir=1'>Pipe Cap</A><BR></li>
	<li><A href='?src=\ref[src];make=35;dir=1'>4-Way Manifold</A><BR></li>
	<li><A href='?src=\ref[src];make=37;dir=1'>Upward Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=39;dir=1'>Downward Pipe</A><BR></li>
</ul>
<b>Scrubbers pipes:</b><BR>
<ul>
	<li><A href='?src=\ref[src];make=31;dir=1'>Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=32;dir=5'>Bent Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=34;dir=1'>Manifold</A><BR></li>
	<li><A href='?src=\ref[src];make=42;dir=1'>Pipe Cap</A><BR></li>
	<li><A href='?src=\ref[src];make=36;dir=1'>4-Way Manifold</A><BR></li>
	<li><A href='?src=\ref[src];make=38;dir=1'>Upward Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=40;dir=1'>Downward Pipe</A><BR></li>
</ul>
<b>Devices:</b><BR>
<ul>
	<li><A href='?src=\ref[src];make=28;dir=1'>Universal pipe adapter</A><BR></li>
	<li><A href='?src=\ref[src];make=4;dir=1'>Connector</A><BR></li>
	<li><A href='?src=\ref[src];make=7;dir=1'>Unary Vent</A><BR></li>
	<li><A href='?src=\ref[src];make=9;dir=1'>Gas Pump</A><BR></li>
	<li><A href='?src=\ref[src];make=15;dir=1'>Pressure Regulator</A><BR></li>
	<li><A href='?src=\ref[src];make=16;dir=1'>High Power Gas Pump</A><BR></li>
	<li><A href='?src=\ref[src];make=10;dir=1'>Scrubber</A><BR></li>
	<li><A href='?src=\ref[src];makemeter=1'>Meter</A><BR></li>
	<li><A href='?src=\ref[src];make=13;dir=1'>Gas Filter</A><BR></li>
	<li><A href='?src=\ref[src];make=23;dir=1'>Gas Filter - Mirrored</A><BR></li>
	<li><A href='?src=\ref[src];make=14;dir=1'>Gas Mixer</A><BR></li>
	<li><A href='?src=\ref[src];make=25;dir=1'>Gas Mixer - Mirrored</A><BR></li>
	<li><A href='?src=\ref[src];make=24;dir=1'>Gas Mixer - T</A><BR></li>
	<li><A href='?src=\ref[src];make=26;dir=1'>Omni Gas Mixer</A><BR></li>
	<li><A href='?src=\ref[src];make=27;dir=1'>Omni Gas Filter</A><BR></li>
</ul>
<b>Heat exchange:</b><BR>
<ul>
	<li><A href='?src=\ref[src];make=2;dir=1'>Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=3;dir=5'>Bent Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=6;dir=1'>Junction</A><BR></li>
	<li><A href='?src=\ref[src];make=17;dir=1'>Heat Exchanger</A><BR></li>
</ul>
<b>Insulated pipes:</b><BR>
<ul>
	<li><A href='?src=\ref[src];make=11;dir=1'>Pipe</A><BR></li>
	<li><A href='?src=\ref[src];make=12;dir=5'>Bent Pipe</A><BR></li>
</ul>
<b>Disposal Pipes</b><br><br>
<ul>
	<li><A href='?src=\ref[src];dmake=0'>Pipe</A><BR></li>
	<li><A href='?src=\ref[src];dmake=1'>Bent Pipe</A><BR></li>
	<li><A href='?src=\ref[src];dmake=2'>Junction</A><BR></li>
	<li><A href='?src=\ref[src];dmake=3'>Y-Junction</A><BR></li>
	<li><A href='?src=\ref[src];dmake=4'>Trunk</A><BR></li>
	<li><A href='?src=\ref[src];dmake=5'>Bin</A><BR></li>
	<li><A href='?src=\ref[src];dmake=6'>Outlet</A><BR></li>
	<li><A href='?src=\ref[src];dmake=7'>Chute</A><BR></li>
	<li><A href='?src=\ref[src];dmake=21'>Upwards</A><BR></li>
	<li><A href='?src=\ref[src];dmake=22'>Downwards</A><BR></li>
</ul>"}

	user << browse(dat, "window=pipedispenser")
	onclose(user, "pipedispenser")
	return

/obj/item/weapon/pipe_dispenser/Topic(href, href_list)
	if(..())
		return
	usr.set_machine(src)
	src.add_fingerprint(usr)
	if(usr.stat || usr.restrained())
		usr << browse(null, "window=pipedispenser")
		return
	if(href_list["make"])
		if(!wait)
			var/p_type = text2num(href_list["make"])
			var/p_dir = text2num(href_list["dir"])
			var/obj/item/pipe/P = new (usr.loc/* src.loc*/, pipe_type=p_type, dir=p_dir)
			P.update()
			P.add_fingerprint(usr)
			wait = 1
			spawn(10)
				wait = 0
	if(href_list["makemeter"])
		if(!wait)
			new /obj/item/pipe_meter(usr.loc/* src.loc*/)
			wait = 1
			spawn(15)
				wait = 0
	if(href_list["dmake"])
		if(!wait)
			var/p_type = text2num(href_list["dmake"])
			var/obj/structure/disposalconstruct/C = new (usr.loc/* src.loc*/)
			switch(p_type)
				if(0)
					C.ptype = 0
				if(1)
					C.ptype = 1
				if(2)
					C.ptype = 2
				if(3)
					C.ptype = 4
				if(4)
					C.ptype = 5
				if(5)
					C.ptype = 6
					C.density = 1
				if(6)
					C.ptype = 7
					C.density = 1
				if(7)
					C.ptype = 8
					C.density = 1
///// Z-Level stuff
				if(21)
					C.ptype = 11
				if(22)
					C.ptype = 12
///// Z-Level stuff
			C.add_fingerprint(usr)
			C.update()
			wait = 1
			spawn(15)
				wait = 0
	return


/obj/item/weapon/pipe_dispenser/afterattack(atom/A, mob/user)
	if(!in_range(A,user))
		return
	if(loc != user)
		return
//	if(!isrobot(user) && !ishuman(user))
//		return 0
	if(istype(A,/area/shuttle)||istype(A,/turf/space/transit))
		return 0
	if(istype(A, /obj/structure/lattice))
		A = get_turf(A)

